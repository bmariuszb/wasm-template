# Rust Wasm Template Project
This repository contains a template project for building Rust WebAssembly applications.

## Using `trunk`
### Clone
```console
git clone -b trunk --single-branch https://gitlab.com/bmariuszb/wasm-template.git
```
### Compile and run
```console
trunk serve --open
```
### Pros
- good for development
- watching files
- live reloading
- live recompilation

## Using `cargo build` with `wasm-bindgen`
### Clone
```console
git clone -b wasm-bindgen --single-branch https://gitlab.com/bmariuszb/wasm-template.git
```
### Compile
```console
cargo build
```
### Generate JS bindings to wasm
```console
wasm-bindgen --target web --out-dir deploy/ --no-typescript target/wasm32-unknown-unknown/<debug-or-release>/<project_name>.wasm
```
### Run
Run HTTP file server and open index.html e.g.  
```console
cd deploy && miniserve --index index.html
```
### Pros
- clean
- flexible - you can serve .html, .wasm and .js files with your backend
- good for deployment
